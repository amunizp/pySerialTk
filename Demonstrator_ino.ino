// Arduino Code:
// Below, 'character' types are defined:  They hold 1 byte of data, 256 values.
// A char can be interpreted as a small number (0-255) or as a member of the
// ASCII set (which is what we deal with below).  Characters expressed as
// ASCII are surrounded in single-quotes, like '5'.
// Thus each char has a corresponding numeric value can thus be tested against.
// http://www.akeric.com/blog/?p=1140


int led13 = 13;
int led12 = 12;
char val = '5'; 

// the setup routine runs once when you press reset:
void setup() {                
  // initialize the digital pin as an output.
  pinMode(led13, OUTPUT);  
  pinMode(led12, OUTPUT);
  
  Serial.begin(9600); // set the baud rate
  Serial.println("Ready"); // print "Ready" once
}


void loop () {
  if (Serial.available()) {
    // For the below examples, let's pretend that the passed-in serial 
    // value is character '5'.
    // Since the declared variable val is an int, it converts the char 
    // value passed in into an int.
    // If char val = '5', the numeric representation is 53.
    val = Serial.read();      // read the serial port

    // If the stored value is a single-digit number, blink the LED 
    // that number of times.
    // Here we compare the int value of val against the int values 
    // of the string.
    // Characters '0' and '9' are equivalent to integer 48 and 57.
    if (val == '0'){
    //if (val > '0' && val <= '9' ) {
      Serial.println(val);
      // Convert from char to int:
      // From above, int conversion of val, which is char '5', is 53.
      // int conversion of char '0' is 48.
      // 53-48 = 5  : blink that # of times
        digitalWrite(led13, HIGH);  // turn the LED on (HIGH is the voltage level)
        digitalWrite(led12, LOW);
        Serial.println("Green is on");
    }
    else if (val == '1'){   
        //delay(2500);   // wait for a second
        digitalWrite(led12, HIGH);  // turn the LED on (HIGH is the voltage level)
        digitalWrite(led13, LOW);
        Serial.println("Red is on");
    }
    else {
      digitalWrite(led13, LOW);    // turn the LED off by making the voltage LOW
        digitalWrite(led12, LOW);
        Serial.println("Light Off");
    }
  }
}
