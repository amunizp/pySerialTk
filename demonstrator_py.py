import tkinter as tk
from tkinter import ttk
import serial
import time

PORT = '/dev/ttyACM0' # The port my Arduino is on, on my computer box


def greenButton():
    serialWrite('0')

def redButton():
    serialWrite('1')

def offButton():
    serialWrite('2')

def serialWrite(val='8'):
    # Open a connection to the serial port.  This will reset the Arduino, and
    # make the LED flash once:
    ser = serial.Serial(PORT)
    value = bytes(val,'UTF-8')
    #print(value)

    # Must given Arduino time to rest.
    # Any time less than this does not seem to work...
    time.sleep(1.5)

    # Now we can start sending data to it:
    written = ser.write(value)
    ser.close()
    print ("Bytes Written to port:", written)
    print ("Value written to port: '%s'"%value)


root = tk.Tk()
ttk.Label(root, text="Press a Button to Continue",).pack(pady=10, padx=10)
ttk.Button(root, text="GREEN", command= greenButton).pack()
ttk.Button(root, text="RED", command=redButton).pack()
ttk.Button(root, text="OFF", command=offButton).pack()


#We need a reference to the widget here, for the state func...

root.mainloop()